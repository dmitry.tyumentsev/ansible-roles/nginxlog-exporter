Nginxlog exporter role
=========

Installs nginxlog exporter for Prometheus on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.nginxlog_exporter
```
